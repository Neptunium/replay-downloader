package com.lesuorac.replay.downloader;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Downloads the replays from entgaming.net
 *
 */
@SpringBootApplication
@EnableEurekaClient
@RestController
@EnableFeignClients
public class ReplayDownloaderApplication {

	/**
	 * The (maximum) amount of time after a connection is made but before data
	 * is read to throw an error (ms)
	 */
	private static final int URL_READ_TIMEOUT = 5 * 1000;

	/**
	 * The (maximum) amount of time it can take to make a conncetion to a URL
	 * (ms)
	 */
	private static final int URL_CONNECTION_TIMEOUT = 15 * 1000;

	private static final Logger LOGGER = LogManager.getFormatterLogger();

	public static void main(String[] args) {
		SpringApplication.run(ReplayDownloaderApplication.class, args);
	}

	@RequestMapping(value = "/replay/download", method = RequestMethod.POST)
	public Long downloadReplay(HttpServletRequest request, @RequestParam("gid") Integer gid) throws IOException {
		Long startTime = System.currentTimeMillis();

		LOGGER.debug("Receving request [%s] from [%s]/[%s]:[%s] @[%d] for gid [%d]", request.getRequestURL(),
				request.getRemoteHost(), request.getRemoteAddr(), request.getRemotePort(), startTime, gid);

		try {
			String downloadLocation = String
					.format("http://storage.entgaming.net/replay/download.php?f=%1$d.w3g&fc=%1$d.w3g", gid);
			String destination = String.format("/media/sf_lesuoracnet/downloadedReplays/%d.w3g", gid);

			FileUtils.deleteQuietly(new File(destination));

			FileUtils.copyURLToFile(new URL(downloadLocation), new File(destination), URL_CONNECTION_TIMEOUT,
					URL_READ_TIMEOUT);
		} catch (IOException e) {
			LOGGER.error("Unable to handle request [%s] from [%s]/[%s]:[%s] for gid [%d] due to exception,",
					request.getRequestURL(), request.getRemoteHost(), request.getRemoteAddr(), request.getRemotePort(),
					gid, e);
			throw e;
		}

		Long endTime = System.currentTimeMillis();

		LOGGER.debug("Handled request [%s] from [%s]/[%s]:[%s] took [%d] ms for gid [%d]", request.getRequestURL(),
				request.getRemoteHost(), request.getRemoteAddr(), request.getRemotePort(), (endTime - startTime), gid);

		return endTime - startTime;
	}
}
